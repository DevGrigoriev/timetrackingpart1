//
//  ViewController.m
//  TimeTrackingPart1
//
//  Created by Admin on 14.05.16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

#import "ViewController.h"
#import "CreateTableCell.h"
#import "CellItem.h"

NSString * TableViewCellIdentifier = @"TableViewCellIdentifier";

@interface ViewController ()
@property (nonatomic, strong) NSMutableArray *items;


@end

@implementation ViewController

-(void)loadView{
    _CreateTable = [[CreateTable alloc]init];
    [self setView:_CreateTable];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = YES;
    _items = [[NSMutableArray alloc] init];
    
    
    UIBarButtonItem *addbut = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd                                                                            target:self
                                                                                action:@selector(playButtonClick)];
    self.navigationItem.leftBarButtonItems = @[addbut];
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.title = @"Main bar";
    
    _calculator = 0;
    
    _CreateTable.table.dataSource = self;
    _CreateTable.table.delegate = self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellItem *item = _items[indexPath.row];
    
    CreateTableCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier];
    if (!cell) {
        cell = [[CreateTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TableViewCellIdentifier];
       [cell.playButton addTarget:self action:@selector(accessoryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    [cell.textLabel setText:item.number.stringValue];

    [cell.playButton setImage:item.play ? [UIImage imageNamed:@"button-play.png"] : [UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
    
    NSString *string = [NSString stringWithFormat:@"%@.png", item.imageNumber];
    UIImage *image = [UIImage imageNamed:string];
    
    [cell.imageView setImage:image];
    
    NSInteger minutes = item.interval / 60;
    NSInteger seconds = item.interval - minutes * 60;
    
    cell.timeLabel.text = [NSString stringWithFormat:@"%02li : %02li", (long)minutes, (long)seconds];
   
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row % 2 == 0 ? 40.0f:40.0f;
}



-(void)playButtonClick{
    CellItem *item = [[CellItem alloc] init];
    item.number = @(_items.count + 1);
    item.play = YES;
    item.imageNumber = @(arc4random_uniform(7));
    
    [_items addObject:item];
    [_CreateTable.table reloadData];
}

- (void)accessoryButtonClick:(UIButton *)sender {
    UIView *superview = sender.superview;
    while (superview && ![superview isKindOfClass:[UITableViewCell class]]) {
        superview = superview.superview;
    }
    if (superview) {
        CreateTableCell *cell = (CreateTableCell *)superview;
        NSIndexPath *indexPath = [_CreateTable.table indexPathForCell:cell];
        
        CellItem *item = [_items objectAtIndex:indexPath.row];
        
        if (item.play) {
            item.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                          target:self
                                                        selector:@selector(timerTick:)
                                                        userInfo:indexPath
                                                         repeats:YES];
        }
        else {
            [item.timer invalidate];
            item.timer = nil;
        }
        
        item.play = !item.play;
  ///??????
    }
    [_CreateTable.table reloadData];
}

- (void)timerTick:(NSTimer *)timer {
    NSIndexPath *indexPath = [timer userInfo];
    CellItem *item = [_items objectAtIndex:indexPath.row];
    item.interval += 1.0f;
    
    [_CreateTable.table beginUpdates];
    [_CreateTable.table reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [_CreateTable.table endUpdates];
}
    



@end
