//
//  CellItem.h
//  TimeTrackingPart1
//
//  Created by Admin on 15.05.16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellItem : NSObject

@property (nonatomic, strong) NSNumber *number;
@property (nonatomic, assign) BOOL play;

@property (nonatomic, strong) NSNumber *imageNumber;

@property (nonatomic, assign) NSTimeInterval interval;

@property (nonatomic, strong) NSTimer *timer;

@end
