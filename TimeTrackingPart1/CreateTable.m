//
//  CreateTable.m
//  TimeTrackingPart1
//
//  Created by Admin on 15.05.16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

#import "CreateTable.h"

@implementation CreateTable

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self addSubview:_table];
    }
    return self;
}
-(void)layoutSubviews{
    [super layoutSubviews];
    
     CGRect originbounds = self.bounds;
    _table.frame = CGRectMake(0, 0, originbounds.size.width, originbounds.size.height);
}
@end
