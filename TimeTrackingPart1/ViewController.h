//
//  ViewController.h
//  TimeTrackingPart1
//
//  Created by Admin on 14.05.16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateTable.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) CreateTable *CreateTable;
@property(nonatomic, assign) NSInteger calculator;

@end

