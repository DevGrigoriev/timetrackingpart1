//
//  CreateTableCell.h
//  TimeTrackingPart1
//
//  Created by Admin on 15.05.16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateTableCell : UITableViewCell;

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIButton *playButton;

@end
