//
//  CreateTableCell.m
//  TimeTrackingPart1
//
//  Created by Admin on 15.05.16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

#import "CreateTableCell.h"

@implementation CreateTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _playButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 20.0f, 20.0f)];
        self.accessoryView = _playButton;
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLabel.text = @"AsdASdasd";
        [self.contentView addSubview:_timeLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _timeLabel.frame = CGRectMake(self.accessoryView.frame.origin.x - 10.0f - 100.0f,
                                  0.0f,
                                  100.0f,
                                  self.bounds.size.height
                                  );
}




@end
